import requests
import json
import apprise
import time
import logging
import logging.handlers
import sys
import os
import datetime
USERNAME = os.environ.get('USERNAME')
PASSWORD = os.environ.get('PASSWORD')
BASE_URL = "https://www.instacart.com/v3/"
STORE_ID = os.environ.get('STORE_ID', 105)
WEBHOOK_URL = os.environ.get('WEBHOOK_URL')

path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler_file = logging.handlers.RotatingFileHandler((dir_path + '/log/instacart.log'),maxBytes=10 * 1024 * 1024 , backupCount=1)
handler_file.setLevel(logging.INFO)
handler_stdout = logging.StreamHandler(sys.stdout)
handler_stdout.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler_stdout.setFormatter(formatter)
handler_file.setFormatter(formatter)
root.addHandler(handler_file)
root.addHandler(handler_stdout)
headers ={
    "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36"
}
def form_url(path):
    return "{}{}".format(BASE_URL,path)
def available_today(availability):
    curr_month = datetime.datetime.now().strftime("%B").lower()[:3]
    window = availability['retailer_availability'][str(STORE_ID)]['window']
    tracking = availability['retailer_availability'][str(STORE_ID)]['tracking_params']
    
    return tracking and curr_month not in window.lower()
    
s = requests.Session()
# Login to instacart
data = {"scope":"","grant_type":"password","signup_v3_endpoints_web":"","email":USERNAME,"password":PASSWORD,"address":""}
# Begin checking availability
r = s.post(form_url("dynamic_data/authenticate/login?source=web&cache_key=undefined"), data=data, headers=headers)
if r.status_code == 200:
    last_availability = None
    while (1):
        # Once logged in, make call to availability endpoint
        path = "retailers/{}/availability?source=web&cache_key=&source1=storefront".format(STORE_ID)
        r = s.get(form_url(path), headers=headers)
        if r.status_code == 200:
            resp = r.json()
            if available_today(resp):
                if resp['retailer_availability'][str(STORE_ID)]['availability'] != last_availability:
                    root.info("There are available time slots for today and they are different than last retrieved! Sending notification")
                    last_availability = resp['retailer_availability'][str(STORE_ID)]['availability']
                    notifier = apprise.Apprise(asset=apprise.AppriseAsset(
                            image_url_mask="https://avatars3.githubusercontent.com/u/3368377?s=200&v=4",
                            default_extension=".jpeg"))
                    notifier.add(WEBHOOK_URL)
                    notifier.notify(title="Instacart Availability", body="Time Window: {}".format(last_availability))
                else:
                    root.info("Timeslots have not changed from last notification. Not bothering to send again")
            else:
                root.info("No available time slots. Checking again in 5 minutes...")
            time.sleep(300)