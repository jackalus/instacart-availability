# Instacart Availability 

David did all the work, I (Jack) added it to git repo. 

# Usage

## Docker

### Build Locally
```
docker build -t instacart .
docker run --rm -e USERNAME='username@gmail.com' -e PASSWORD='PickleRick' -e WEBHOOK_URL='ifttt:///token@event/' instacart
```
### docker run
```
docker run --rm -e USERNAME='username@gmail.com' -e PASSWORD='PickleRick' -e WEBHOOK_URL='ifttt:///token@event/' registry.gitlab.com/jackalus/instacart-availability:master
```
### docker-compose
```
version: "3.7"
services:
  instacart:
    image: registry.gitlab.com/jackalus/instacart-availability:master
    environment: 
      - USERNAME=username@gmail.com
      - PASSWORD=PickleRick
      - WEBHOOK_URL=ifttt:///token@event/
```