FROM python:3-alpine
ENV USERNAME '' \
    PASSWORD '' \
    WEBHOOK_URL ''
WORKDIR /app
COPY . .
RUN pip3 install --no-cache-dir -r requirements.txt
VOLUME /app/log
CMD python3 ./instacart.py